//  Created by Александр Холодов on 9/24/16.
//  Copyright © 2016 Александр Холодов. All rights reserved.

//import UIKit

// import Foundation

//do {let arguments = try CommandLine.arguments}

// let count = arguments ?? arguments,  "нет агрументов"

//var c = 0;
//for arg in CommandLine.arguments {
//    print("argument \(c) is: \(arg)")
//    c += 1
//}

// напишите функцию площади круга, где Пи вычисляется во внутреней функции

// Пи = 4 - 4/3 + 4/5 - 4/7 + 4/9
func piSumple() -> Double {
    return 4.0 - (4 / 3) + (4 / 5) - (4 / 7) + (4 / 9) - (4 / 11) + (4 / 13)
}
print(piSumple())

func pi(iter: Int = 100) -> Double {
    
    var pi = 4.0
    var even = 1
    
//    for i in 3...100 {
    for i in stride(from:3, to: iter * 2 + 2, by:2) {
        if (even % 2) > 0 {
            pi = pi - (4.0 / Double(i))
        } else {
            pi = pi + (4.0 / Double(i))
        }
        print(i, even, pi)
        even += 1
    }
    return pi
}
print(pi(iter: 50))


// вычислить Пи до указанного знака (домашка)


func module(number: Double) -> Double {
    return number >= 0.0 ? number : -number
}

func module(number: Int) -> Int {
    return number >= 0 ? number : -number
}

print(module(number: -1.0))
print(module(number: 1.0))
print(module(number: -1))


func power(number: Int = 10, exp: Int = 2) -> Int {
    switch exp {
    case 1:
        return number
    case 2..<Int.max:
        var pow = number
        for _ in 1...(exp - 1)  {
            pow *= number
        }
        return pow
    default:
        return 1
    }
}
power(number: 5, exp: 3)

func comparePi (pi: Double, piPrev: Double, prec: Int) -> Bool {
    let piInt = Int (pi * Double(power(exp: prec)))
    let piPrevInt = Int (piPrev * Double(power(exp: prec)))
    return module(number: piInt - piPrevInt) > 0 ? true : false
}

func piPrec(prec: Int = 1 ) -> Double {
    
    var pi = 4.0
    var piPrev = 0.0
    var even = 1
    var i = 3
    
    while comparePi(pi: pi, piPrev: piPrev, prec: prec) {
        piPrev = pi
        if (even % 2) > 0 {
            pi = pi - (4.0 / Double(i))
        } else {
            pi = pi + (4.0 / Double(i))
        }
        print(i, even, pi, piPrev, module(number: (pi - piPrev)))
        even += 1
        i += 2
    }
    return pi
}
print(piPrec(prec: 1))